import React from 'react'
import { RegisterForm } from './components/RegisterForm'

export const Register = () => (
    <div>
        <div className="wrapper">
            <RegisterForm/>
        </div>
    </div>
)